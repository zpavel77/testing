import pathlib
import sys

flist = []

name = sys.argv[1]

for p in pathlib.Path(name).iterdir():
    if p.is_file():
        flist.append((str(p),p.stat().st_mtime))

sort_list = sorted (flist, key=lambda modify: modify[1], reverse=True)

for i in range(len(sort_list)):
    if i < 5:
        continue

    pathlib.Path('./'+sort_list[i][0]).unlink()



